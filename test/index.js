import { assert, expect } from 'chai';
import cpfValidator from '../src';

describe('CPF validator', () => {
  it('10533953707 should be valid', () => {
    const result = cpfValidator.validate('10533953707');
    expect(result).to.equal(true)
  });

  it('105.339.537-07 should be valid', () => {
    const result = cpfValidator.validate('105.339.537-07');
    expect(result).to.equal(true)
  });

  it('10533953706 should not be valid', () => {
    const result = cpfValidator.validate('10533953706');
    expect(result).to.equal(false)
  });

  it('105.339.537-06 should not be valid', () => {
    const result = cpfValidator.validate('105.339.537-06');
    expect(result).to.equal(false)
  });

  it('11111111111 should not be valid', () => {
    const result = cpfValidator.validate('11111111111');
    expect(result).to.equal(false)
  });

  it('111.111.111-11 should not be valid', () => {
    const result = cpfValidator.validate('111.111.111-11');
    expect(result).to.equal(false)
  });
});
