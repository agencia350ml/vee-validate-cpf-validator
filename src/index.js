const cpfValidator = {
  getMessage(field) {
    const errorMessage = `The ${field} is not valid.`;
    return errorMessage;
  },
  validate(value) {
    let cpf = value.replace(/\D/g,'');
    let expReg = /^0+$|^1+$|^2+$|^3+$|^4+$|^5+$|^6+$|^7+$|^8+$|^9+$/;
    let a = [];
    let b = new Number;
    let c = 11;
    let x = 0;
    while(cpf.length < 11) cpf = `0${cpf}`;
    for (var i=0; i<11; i+=1){
        a[i] = cpf.charAt(i);
        if (i < 9) b += (a[i] * --c);
    }
    if ((x = b % 11) < 2) { a[9] = 0 } else { a[9] = 11-x }
    b = 0;
    c = 11;
    for (var y=0; y<10; y++) b += (a[y] * c--);
    if ((x = b % 11) < 2) { a[10] = 0; } else { a[10] = 11-x; }
    if ((cpf.charAt(9) != a[9]) || (cpf.charAt(10) != a[10]) || cpf.match(expReg)) return false;
    return true;
  }
};

export default cpfValidator;
